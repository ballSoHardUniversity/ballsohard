/* Group Name: BallSoHardUniversity
 * Group Members: Collin Murphy, Michael Edwards, Stephen Worlow 
 * Group Motto: Get on our level
 */

package edu.caltech.cs141b.hw2.gwt.collab.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.jdo.PersistenceManager;


import edu.caltech.cs141b.hw2.data.Document;
import edu.caltech.cs141b.hw2.gwt.collab.client.CollaboratorService;
import edu.caltech.cs141b.hw2.gwt.collab.shared.DocumentMetadata;
import edu.caltech.cs141b.hw2.gwt.collab.shared.LockExpired;
import edu.caltech.cs141b.hw2.gwt.collab.shared.LockUnavailable;
import edu.caltech.cs141b.hw2.gwt.collab.shared.LockedDocument;
import edu.caltech.cs141b.hw2.gwt.collab.shared.UnlockedDocument;

import com.google.appengine.api.datastore.KeyFactory;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class CollaboratorServiceImpl extends RemoteServiceServlet implements
		CollaboratorService {
	
	// private static final Logger log = Logger.getLogger(CollaboratorServiceImpl.class.toString());

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentMetadata> getDocumentList() {
		ArrayList<DocumentMetadata> docs = new ArrayList<DocumentMetadata>();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query q = pm.newQuery(Document.class);
		Transaction t = pm.currentTransaction();
		try { 
			t.begin();
			List<Document> origDoc = (List<Document>) q.execute();
			for(int i = 0; i < origDoc.size(); i++)
				docs.add(new DocumentMetadata(origDoc.get(i).getKey(),origDoc.get(i).getTitle()));
			t.commit();
		}
		finally {
			if (t.isActive())
				t.rollback();
			q.closeAll();
			pm.close();
		}
		return docs;
	}
	@Override
	public LockedDocument lockDocument(String documentKey)
			throws LockUnavailable {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Transaction t = pm.currentTransaction();
		Document doc;
		try {
			t.begin();
			doc = pm.getObjectById(Document.class, KeyFactory.stringToKey(documentKey));
			if (doc.isLocked()) {
				if (doc.getLockedUntil().before(new Date(System.currentTimeMillis())))
					doc.unlock();
				else
					throw new LockUnavailable();
			}
			doc.lock(getThreadLocalRequest().getRemoteAddr(), new Date(System.currentTimeMillis()+doc.getTimeLock()));
			pm.makePersistent(doc);
			t.commit();
		}
		finally {
			if (t.isActive())
				t.rollback();
			pm.close();
		}
		return doc.getLockedDocument();
	}
	@Override
	public UnlockedDocument getDocument(String documentKey) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Transaction t = pm.currentTransaction();
		Document doc;
		try {
			t.begin();
			doc = pm.getObjectById(Document.class, KeyFactory.stringToKey(documentKey));
			t.commit();
		}
		finally {
			if (t.isActive())
				t.rollback();
			pm.close();
		}
		return doc.getUnlockedDocument();
	}
	@Override
	public UnlockedDocument saveDocument(LockedDocument doc)
			throws LockExpired {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Transaction t = pm.currentTransaction();
		Document docs;
		String keys = doc.getKey();
		try {
			t.begin();
			if (keys == null)
				docs = new Document(doc.unlock());
			else {
				docs = pm.getObjectById(Document.class, KeyFactory.stringToKey(keys));
				if (docs.getLockedBy().equals(getThreadLocalRequest().getRemoteAddr()) && docs.getLockedUntil().after(new Date(System.currentTimeMillis()))) {
					docs.save(doc);
					docs.unlock();
				}
				else
					throw new LockExpired();
			}
			pm.makePersistent(docs);
			t.commit();
		}
		finally {
			if (t.isActive())
				t.rollback();
			pm.close();
		}
		return docs.getUnlockedDocument();
	}
	@Override
	public void releaseLock(LockedDocument doc) throws LockExpired {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Document docs;
		String keys = doc.getKey();
		Transaction t = pm.currentTransaction();
		try {
			t.begin();
			docs = pm.getObjectById(Document.class, KeyFactory.stringToKey(keys));
			docs.unlock();
			pm.makePersistent(docs);
			t.commit();	
		}
		finally {
			if (t.isActive())
				t.rollback();
			pm.close();
		}
	}
}