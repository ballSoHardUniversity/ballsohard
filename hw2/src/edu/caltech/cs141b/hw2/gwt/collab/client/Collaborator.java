package edu.caltech.cs141b.hw2.gwt.collab.client;

import java.util.ArrayList;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.caltech.cs141b.hw2.gwt.collab.shared.LockedDocument;
import edu.caltech.cs141b.hw2.gwt.collab.shared.UnlockedDocument;

/**
 * Main class for a single Collaborator widget.
 */
public class Collaborator extends Composite implements ClickHandler, ChangeHandler {
	
	protected CollaboratorServiceAsync collabService;
	
	// Track document information.
	protected UnlockedDocument readOnlyDoc = null;
	protected LockedDocument lockedDoc = null;
	
	// List of tab content
	protected ArrayList<TabContent> tabContents = new ArrayList<TabContent>();
	
	// Managing available documents.
	protected ListBox documentList = new ListBox();
	private Button refreshList = new Button("Refresh Document List");
	private Button createNew = new Button("Create New Document");
	
	// For displaying document information and editing document content.
	protected TextBox title = new TextBox();
	protected RichTextArea contents = new RichTextArea();
	protected Button refreshDoc = new Button("Refresh Document");
	protected Button lockButton = new Button("Get Document Lock");
	protected Button saveButton = new Button("Save Document");
	
	protected TabLayoutPanel tabPanel = new TabLayoutPanel(3.5,Unit.EM);
	
	// Callback objects.
	protected DocLister lister = new DocLister(this);
	protected DocReader reader = new DocReader(this);
	private DocLocker locker = new DocLocker(this);
	protected DocReleaser releaser = new DocReleaser(this);
	private DocSaver saver = new DocSaver(this);
	protected String waitingKey = null;
	
	// Status tracking.
	private VerticalPanel statusArea = new VerticalPanel();
	private VerticalPanel outerVp_1;
	private final VerticalPanel verticalPanel_1 = new VerticalPanel();

	private final HorizontalPanel outerHp;
	private final Button closeTabButton = new Button("Close Tab");
	
	/**
	 * UI initialization.
	 * 
	 * @param collabService
	 */
	public Collaborator(CollaboratorServiceAsync collabService){
		this.collabService = collabService;
		outerHp = new HorizontalPanel();
		outerVp_1 = new VerticalPanel();
		outerVp_1.add(lockButton);
		lockButton.addClickHandler(this);
		outerVp_1.add(saveButton);
		saveButton.addClickHandler(this);
		outerVp_1.add(refreshDoc);
		refreshDoc.addClickHandler(this);
		outerVp_1.add(closeTabButton);
		closeTabButton.addClickHandler(this);
		outerVp_1.add(createNew);
		createNew.addClickHandler(this);
		outerVp_1.add(refreshList);
		refreshList.addClickHandler(this);
		
		
		
		VerticalPanel vp = new VerticalPanel();
		outerVp_1.add(vp);
		vp.setSpacing(15);
		vp.add(new HTML("<h2>Current Documents</h2>"));
		documentList.setWidth("100%");
		vp.add(documentList);
		
		documentList.addChangeHandler(this);
		documentList.setVisibleItemCount(10);
		
		outerHp.add(outerVp_1);
		outerHp.add(tabPanel);
		tabPanel.setSize("860px", "888px");
		
		VerticalPanel startPanel = new VerticalPanel();
		startPanel.add(new HTML("Start Your Collab Adventure Here."));
		tabPanel.add(startPanel, new HTML("Start"));
		tabPanel.selectTab(0);
		
		
		
		tabPanel.addSelectionHandler(new SelectionHandler<Integer>(){
			@Override
			public void onSelection(SelectionEvent<Integer> event){
				int tabIndex = event.getSelectedItem();
				if(tabIndex == 0){
					lockButton.setEnabled(false);
					saveButton.setEnabled(false);
					refreshDoc.setEnabled(false);
					closeTabButton.setEnabled(false);
				}
				else{
					TabContent current = tabContents.get(tabIndex-1);
					
					readOnlyDoc = current.getReadOnlyDoc();
					lockedDoc = current.getLockedDoc();
					title = current.getTitle();
					contents = current.getContents();
					lockButton.setEnabled(current.getLockButton());
					saveButton.setEnabled(current.getSaveButton());
					refreshDoc.setEnabled(current.getRefreshDoc());
					
					
					if(current.getKey() == null){
						title.setEnabled(true);
						contents.setEnabled(true);
					}
					closeTabButton.setEnabled(true);
				}
			}
		});
		
		
		setDefaultButtons();
		initWidget(outerHp);
		outerHp.add(verticalPanel_1);
		verticalPanel_1.setSpacing(15);
		verticalPanel_1.add(new HTML("<h3>Console</h3>"));
		verticalPanel_1.add(statusArea);
		statusArea.setSpacing(15);
		
		lister.getDocumentList();
		}
	
	
	/**
	 * Find index of document from the key in tabPanel.
	 * @param currKey
	 * @return docIndex
	 */
	protected int getDocIndex(String currKey){
		int docIndex;
		for(docIndex=0; docIndex<tabContents.size(); docIndex++){
			String key = tabContents.get(docIndex).getKey();
			if(key!=null && key.equals(currKey)){
				return docIndex;
			}
			else if(key==null && currKey==null){
				return docIndex;
			}
		}
		return docIndex;
	}
	
	
	/**
	 * Open tab or create new one
	 */
	protected int getTab(String currKey, String currTitle){
		int docIndex = getDocIndex(currKey);
		
		//get tab
		TabContent tab = null;
		
		if(docIndex==tabContents.size()){
			tab = new TabContent(null, null);
			tabContents.add(tab);
			
			tabPanel.add(tab.getVp(), currTitle);
		}
		else{
			tab = tabContents.get(docIndex);
		}
		if(readOnlyDoc==null){
			tab.setReadOnlyDoc(null);
		}
		else{
			tab.setReadOnlyDoc(readOnlyDoc.copy());
		}
		if(lockedDoc==null){
			tab.setLockedDoc(null);
		}
		else{
			tab.setLockedDoc(lockedDoc.copy());
		}
		title = tab.getTitle();
		contents = tab.getContents();
		tab.setKey(currKey);
		return docIndex;
	}
	
	/**
	 * Resets the state of the buttons and edit objects to their default.
	 * 
	 * The state of these objects is modified by requesting or obtaining locks
	 * and trying to or successfully saving.
	 */
	protected void setDefaultButtons() {
		refreshDoc.setEnabled(true);
		lockButton.setEnabled(true);
		saveButton.setEnabled(false);
		title.setEnabled(false);
		contents.setEnabled(false);
	}
	
	/**
	 * Behaves similarly to locking a document, except without a key/lock obj.
	 */
	private void createNewDocument() {
		discardExisting(null);
		lockedDoc = new LockedDocument(null, null, null,
				"Enter title.",
				"Write contents.");
		locker.gotDoc(lockedDoc);
		History.newItem("new");
		createNew.setEnabled(false);
	}
	
	/**
	 * Returns the currently active token.
	 * 
	 * @return history token which describes the current state
	 */
	protected String getToken() {
		if (lockedDoc != null) {
			if (lockedDoc.getKey() == null) {
				return "new";
			}
			return lockedDoc.getKey();
		} else if (readOnlyDoc != null) {
			return readOnlyDoc.getKey();
		} else {
			return "list";
		}
	}
	
	/**
	 * Modifies the current state to reflect the supplied token.
	 * 
	 * @param args history token received
	 */
	protected void receiveArgs(String args) {
		if (args.equals("list")) {
			
		} else if (args.equals("new")) {
			createNewDocument();
		} else {
			reader.getDocument(args);
		}
	}
	
	/**
	 * Adds status lines to the console window to enable transparency of the
	 * underlying processes.
	 * 
	 * @param status the status to add to the console window
	 */
	protected void statusUpdate(String status) {
		while (statusArea.getWidgetCount() > 22) {
			statusArea.remove(1);
		}
		final HTML statusUpd = new HTML(status);
		statusArea.add(statusUpd);
	}

	/* (non-Javadoc)
	 * Receives button events.
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	public void onClick(ClickEvent event) {
		if (event.getSource().equals(refreshList)) {
			History.newItem("list");
			lister.getDocumentList();
		} else if (event.getSource().equals(createNew)) {
			createNewDocument();
		} else if (event.getSource().equals(refreshDoc)) {
			if (readOnlyDoc != null) {
				reader.getDocument(readOnlyDoc.getKey());
			}
		} else if (event.getSource().equals(lockButton)) {
			if (readOnlyDoc != null) {
				locker.lockDocument(readOnlyDoc.getKey());
			}
		} else if (event.getSource().equals(saveButton)) {
			if (lockedDoc != null) {
				if (lockedDoc.getTitle().equals(title.getValue()) &&
						lockedDoc.getContents().equals(contents.getHTML())) {
					statusUpdate("No document changes; not saving.");
				}
				else {
					lockedDoc.setTitle(title.getValue());
					lockedDoc.setContents(contents.getHTML());
					saver.saveDocument(lockedDoc);
				}
			}
		}
		else if(event.getSource().equals(closeTabButton)){
			Boolean error = true;
			String key = null;
			if(readOnlyDoc!=null){
				key = readOnlyDoc.getKey();
				error = false;
			}
			else if(lockedDoc!=null){
				key = lockedDoc.getKey();
				error = false;
			}
			//remove tab if possible
			if(!error && tabContents.size()>0){
				int docIndex = getDocIndex(key);
				tabPanel.remove(tabContents.get(docIndex).getVp());
				tabContents.remove(docIndex);
				
				tabPanel.selectTab(tabContents.size());
				if (key == null){
					createNew.setEnabled(true);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * Intercepts events from the list box.
	 * @see com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt.event.dom.client.ChangeEvent)
	 */
	public void onChange(ChangeEvent event) {
		if (event.getSource().equals(documentList)) {
			String key = documentList.getValue(documentList.getSelectedIndex());
			discardExisting(key);
			reader.getDocument(key);
		}
	}
	
	/**
	 * Used to release existing locks when the active document changes.
	 * 
	 * @param key the key of the new active document or null for a new document
	 */
	private void discardExisting(String key) {
		if (lockedDoc != null) {
			if (lockedDoc.getKey() == null) {
				statusUpdate("Discarding new document.");
			}
			else if (!lockedDoc.getKey().equals(key)) {
				releaser.releaseLock(lockedDoc);
			}
			else {
				// Newly active item is the currently locked item.
				return;
			}
			lockedDoc = null;
			setDefaultButtons();
		} else if (readOnlyDoc != null) {
			if (readOnlyDoc.getKey().equals(key)) return;
			readOnlyDoc = null;
		}
	}
}
