/* Group Name: BallSoHardUniversity
 * Group Members: Collin Murphy, Michael Edwards, Stephen Worlow 
 * Group Motto: Get on our level
 */

package edu.caltech.cs141b.hw2.data;

import java.util.Date;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import edu.caltech.cs141b.hw2.gwt.collab.shared.LockedDocument;
import edu.caltech.cs141b.hw2.gwt.collab.shared.UnlockedDocument;

@PersistenceCapable
public class Document {
	@Persistent private static final int timeLock = 10000;
	@Persistent private String title;
	@Persistent private String contents;
	@Persistent private String lockedBy;
	@Persistent private Date lockedUntil;
	@PrimaryKey @Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY) private Key key; //Automatically generates key
	public Document(UnlockedDocument u) {
		this.title = u.getTitle();
		this.contents = u.getContents();
		this.lockedBy = null;
		this.lockedUntil = null;
	}
	public Document(LockedDocument l) {
		this.lockedBy = l.getLockedBy();
		this.lockedUntil = l.getLockedUntil();
		this.title = l.getTitle();
		this.contents = l.getContents();
	}
	public void unlock() {
		this.lockedBy = null;
		this.lockedUntil = null;
	}
	public void lock(String lockedBy, Date lockedUntil) {
		this.lockedBy = lockedBy;
		this.lockedUntil = lockedUntil;
	}
	public void save(LockedDocument l) {
		this.title = l.getTitle();
		this.contents = l.getContents();
	}
	public boolean isLocked() { return lockedBy != null; }
	public int getTimeLock() { return timeLock; }
	public String getTitle() { return title; }
	public String getContents() { return contents; }
	public String getLockedBy() { return lockedBy; }
	public Date getLockedUntil() { return lockedUntil; }
	public String getKey() { return KeyFactory.keyToString(this.key); }
	public UnlockedDocument getUnlockedDocument() {
		return new UnlockedDocument(KeyFactory.keyToString(this.key), title, contents);
	}
	public LockedDocument getLockedDocument() {
		return new LockedDocument(lockedBy, lockedUntil, KeyFactory.keyToString(this.key), title, contents);
	}
	
}